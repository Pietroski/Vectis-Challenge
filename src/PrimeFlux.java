import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class PrimeFlux {
    private long counter = 21L;
    private static final int LIMIT = 100_003;
    private static final Long[] INITIAL_PRIME_LIST = new Long[]{2L, 3L, 5L, 7L, 11L, 13L, 17L, 19L};
    private static ArrayList<Long> primesList = new ArrayList<>(Arrays.asList(INITIAL_PRIME_LIST));
    private static long primeListSum;

    PrimeFlux() {

    }

    void setPrimeNumbers() {
        int primeListSize = primesList.size();

        while (primeListSize < LIMIT) {
            double operation = counter / 2;

            Iterator<Long> primeNumbersList = primesList.iterator();
            primeNumbersList.next();

            while (primeNumbersList.hasNext()) {
                long primeNumber = primeNumbersList.next();

                if (primeNumber > operation) {
                    primesList.add(counter);
                    break;
                }

                boolean isNotPrime = counter % primeNumber == 0;
                if (isNotPrime) {
                    break;
                }
            }

            primeListSize = primesList.size();
            counter += 2;
        }
    }

    void printPrimesList() {
        Iterator<Long> primesListIterator = primesList.iterator();

        while (primesListIterator.hasNext()) {
            System.out.println(primesListIterator.next());
        }

        System.out.println("Prime List Size -> " + primesList.size());
    }

    void getPrimeListSummed() {
        primeListSum = 0;

        Iterator<Long> primeNumbers = primesList.iterator();
        while (primeNumbers.hasNext()) {
            long primeNumber = primeNumbers.next();
            primeListSum += primeNumber;
        }

        System.out.println("Sum of the first 100,003 prime numbers -> " + primeListSum);
    }

    Long getNumber(long numberToCut) {
        String numberToString = Long.toString(numberToCut);
        int stringsLength = numberToString.length();

        int start = stringsLength - 3;
        int end = stringsLength;

        String threeRightMostDigitsString = numberToString.substring(start, end);

        long threeRightMostDigits = Long.parseLong(threeRightMostDigitsString);

        return threeRightMostDigits;
    }

    HashSet<Long> getDigits(long numberToSplit) {
        String stringNumber = Long.toString(numberToSplit);
        int stringNumbersLength = stringNumber.length();
        HashSet<Long> digits = new HashSet<>();

        for (int index = 0; index < stringNumbersLength; index++) {
            char digit = stringNumber.charAt(index);
            long parsedChar = Long.parseLong(Character.toString(digit));
            digits.add(parsedChar);
        }

        return digits;
    }

    long getTheLargestNumberFrom (HashSet<Long> hashSet) {
        long largestNumber = 0L;
        Iterator<Long> numberIterator = hashSet.iterator();
        while (numberIterator.hasNext()) {
            long number = numberIterator.next();
            if (number > largestNumber) {
                largestNumber = number;
            }
        }

        return largestNumber;
    }

    HashSet<Long> getPrimeFactors(long number) {
        long numberToPrimeFactor = getNumber(number);
        HashSet<Long> digits = getDigits(numberToPrimeFactor);
        digits.add(numberToPrimeFactor);

        HashSet<Long> primeFactors = new HashSet<>();

        Iterator<Long> digitsIterator = digits.iterator();
        while (digitsIterator.hasNext()) {
            long nominator = digitsIterator.next();
            long rest = 0L;

            Iterator<Long> primeNumbers = primesList.iterator();
            long prime = primeNumbers.next();
            while (primeNumbers.hasNext()) {

                if (nominator == prime) {
                    primeFactors.add(prime);
                    break;
                }

                if (nominator % prime == 0) {
                    rest = nominator / prime;
                    nominator = rest;

                    primeFactors.add(prime);
                    continue;
                }

                prime = primeNumbers.next();
            }
        }

        System.out.println();

        System.out.println(
                "The three most-right digits (but not necessarily in sequence) in the sum of the "
                        + LIMIT + " are -> " + digits
        );

        System.out.println();

        System.out.println(
                "Prime Factors of the three most-right digits in the sum of the "
                        + LIMIT + " first prime numbers -> " + primeFactors
        );

        System.out.println();

        final long LARGEST_PRIME_FACTOR = getTheLargestNumberFrom(primeFactors);

        System.out.println(
                "Hence, the largest prime factor is -> " + LARGEST_PRIME_FACTOR
        );

        return primeFactors;
    }

    void showLargestPrimeFactorOfChallenge() {
        HashSet<Long> number = getPrimeFactors(primeListSum);
    }

    void execute() {
        TimeCounter counter = new TimeCounter(TimeScale.SECONDS);

        counter.start();

        setPrimeNumbers();
        getPrimeListSummed();
        showLargestPrimeFactorOfChallenge();

        counter.end();
        counter.getTime();

    }

    public static void main(String[] args) {
        PrimeFlux primeFlux = new PrimeFlux();
        primeFlux.execute();
    }
}
