public enum TimeScale {
    HOURS("hours"),
    MINUTES("minutes"),
    SECONDS("seconds"),
    MILLISECONDS("milliseconds"),
    NANOSECONDS("nanoseconds");

    private final String timeScale;

    TimeScale(String timeScale) {
        this.timeScale = timeScale;
    }

    @Override
    public String toString() {
        return timeScale;
    }
}
