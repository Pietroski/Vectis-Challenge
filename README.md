# Vectis Challenge

This is a simple code answer to the challenge proposed by [Vectis](https://vectis.ai).

## Challenge considerations

The average execution time of my algorithm is about 23-25 seconds. I could even have saved some milliseconds, but I decided no to do that due to code readability and keeping a function of a higher order.

It is not ideal, but it considers the whole number and its decomposition by digits to be factored.

### Output example
![alt text](./assets/ExecutionSample_001.png)

# About me...
Please, check my curriculum... -> [Augusto Pietroski - Curriculum](./assets/EnglishCurriculum.pdf)

This repo is on -> [Vectis Challenge Repo](https://gitlab.com/Pietroski/Vectis-Challenge).

Please, any doubts consider contacting me via e-mail at AugustoPTSantos@gmail.com

Yours sincerely, 

Augusto Pietroski